package br.com.walmart.rest;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.walmart.bean.Entrega;
import br.com.walmart.business.EntregaBO;

@Controller

public class EntregaRest {

	@RequestMapping(value="/todasEntregas", method = RequestMethod.GET)
	public @ResponseBody List<Entrega> getEntregas(ModelMap model) {
		EntregaBO entregaBO = new EntregaBO();
		return entregaBO.listarEntregas();
	}
	
	@RequestMapping(value="/incluirEntrega")
	public @ResponseBody String incluirEntrega(@RequestParam("nomeMapa") String nomeMapa, 
			@RequestParam("origem") String origem, 
			@RequestParam("destino") String destino,
			@RequestParam("autonomia") float autonomia,
			@RequestParam("vlLitro") float vlLitro) {
		
		
		EntregaBO entregaBO = new EntregaBO();
		
		Entrega entrega = new Entrega();
		entrega.setNomeMapa(nomeMapa);
		entrega.setOrigem(origem);
		entrega.setDestino(destino);
		entrega.setMelhorCaminho(entregaBO.distanciaDaOrigemDestino(entrega));
		entrega.setDistancia(entrega.getMelhorCaminho().getDistancia());
		entrega.setAutonomia(autonomia);
		entrega.setValorLitro(vlLitro);
		entrega.setCusto(entregaBO.calcularEntrega(entrega));
		entregaBO.incluirEntrega(entrega);
		if(entrega.getMelhorCaminho().getParada() != null ){
			return mensagemRetornoComPontoParada(entrega);
		}else{
			return mensagemRetornoSemPontoParada(entrega);
		}
		
	}

	/**
	 * Mensagem de Retorno sem um ponto de Parada
	 * @param entrega
	 * @return
	 */
	private String mensagemRetornoSemPontoParada(Entrega entrega) { 
		return "a rota " + entrega.getMelhorCaminho().getPontoInicial() +
				" " +entrega.getMelhorCaminho().getPontoFinal() +
				" com custo de R$: " + entrega.getCusto()   +" Reais";
	}

	/**
	 * Mensagem de Retorno com um ponto de Parada
	 * @param entrega
	 * @return
	 */
	private String mensagemRetornoComPontoParada(Entrega entrega) {
		return "a rota " + entrega.getMelhorCaminho().getPontoInicial() +
				" " +entrega.getMelhorCaminho().getParada() +
				" " +entrega.getMelhorCaminho().getPontoFinal() +
				" com custo de R$: " + entrega.getCusto()   +" Reais";
	}
	
	
	
}
