/**
 * 
 */
package br.com.walmart.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Samuel Oliveira Santos
 *
 */
@XmlRootElement
public class Caminho implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String pontoInicial;
	private String parada;
	private String pontoFinal;
	private float distancia;
	
	
	
	public Caminho() {
		super();
	}
	/**
	 * @return the pontoInicial
	 */
	public String getPontoInicial() {
		return pontoInicial;
	}
	/**
	 * @param pontoInicial the pontoInicial to set
	 */
	public void setPontoInicial(String pontoInicial) {
		this.pontoInicial = pontoInicial;
	}
	/**
	 * @return the parada
	 */
	public String getParada() {
		return parada;
	}
	/**
	 * @param parada the parada to set
	 */
	public void setParada(String parada) {
		this.parada = parada;
	}
	/**
	 * @return the pontoFinal
	 */
	public String getPontoFinal() {
		return pontoFinal;
	}
	/**
	 * @param pontoFinal the pontoFinal to set
	 */
	public void setPontoFinal(String pontoFinal) {
		this.pontoFinal = pontoFinal;
	}
	/**
	 * @return the distancia
	 */
	public float getDistancia() {
		return distancia;
	}
	/**
	 * @param distancia the distancia to set
	 */
	public void setDistancia(float distancia) {
		this.distancia = distancia;
	}
	
	
	
}
